package org.dbserver.desafiovotacao.v1.models;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tb_topic")
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Data
public class Topic {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  private String title;

  private String description;

  @Enumerated(EnumType.STRING)
  @Builder.Default
  private TopicStatus topicStatus = TopicStatus.STARTED;

  @OneToOne(mappedBy = "topic")
  @JsonBackReference
  private VotingSession votingSession;

  @CreationTimestamp
  private LocalDateTime createdAt;

}
