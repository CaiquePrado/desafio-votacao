package org.dbserver.desafiovotacao.v1.enums;

public enum VoteDecision {
  YES,
  NO
}
