package org.dbserver.desafiovotacao.v1.usecases.user.impl;

import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.DeleteUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DeleteUserByCpfUseCaseImpl implements DeleteUserByCpfUseCase {

  private final UserRepository userRepository;
  private final FindUserByCpfUseCase findUserByCpfUseCase;

  @Override
  public void execute(String cpf) {
    var user = findUserByCpfUseCase.find(cpf);
    userRepository.delete(user);
  }
}
