package org.dbserver.desafiovotacao.v1.usecases.vote;

import java.util.UUID;

public interface VoteValidationUseCase {

  void validate(final String cpf, final UUID sessionId);

}
