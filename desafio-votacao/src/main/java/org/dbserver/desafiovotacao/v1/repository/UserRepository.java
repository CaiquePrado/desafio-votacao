package org.dbserver.desafiovotacao.v1.repository;

import java.util.Optional;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

  Optional<User> findByCpf(String cpf);

  @Query("SELECT u FROM User u")
  Page<User> findByPage(Pageable pageable);

}
