package org.dbserver.desafiovotacao.v1.controllers;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.requests.CreateVoteRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.VoteResultResponseDTO;
import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;

public interface VoteController {

  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Vote created successfully", content = @Content(schema = @Schema(implementation = VoteResultResponseDTO.class))),
      @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "Create a vote")
  ResponseEntity<VoteResultResponseDTO> createVote(final UUID sessionId, @Valid CreateVoteRequestDTO createVoteRequestDTO);

}
