package org.dbserver.desafiovotacao.v1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class SwaggerConfig {

  @Bean
  public OpenAPI apiInfo() {
    return new OpenAPI().info(new Info()
        .title("Challenge Voting")
        .version("1.0")
        .description("Challenge Proposed by the AR DB Unit")
        .contact(new Contact()
            .name("Support")
            .email("caique.santos@db.tec.br"))
        .license(new License()
            .name("APACHE 2.0")
            .url("https://www.apache.org/licenses/LICENSE-2.0")));
  }
}
