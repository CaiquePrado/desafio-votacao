package org.dbserver.desafiovotacao.v1.controllers.impl;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.controllers.VoteController;
import org.dbserver.desafiovotacao.v1.mapper.VoteMapper;
import org.dbserver.desafiovotacao.v1.requests.CreateVoteRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.VoteResultResponseDTO;
import org.dbserver.desafiovotacao.v1.usecases.vote.VoteInSessionUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/vote")
@RequiredArgsConstructor
@Validated
@Tag(name = "Vote", description = "Endpoints for managing votes.")
public class VoteControllerImpl implements VoteController {

  private final VoteInSessionUseCase voteInSessionUseCase;
  private final VoteMapper voteMapper;

  @Override
  @PostMapping("/{sessionId}")
  public ResponseEntity<VoteResultResponseDTO> createVote(@PathVariable UUID sessionId, @RequestBody @Valid CreateVoteRequestDTO createVoteRequestDTO) {
    var vote = voteMapper.toVote(createVoteRequestDTO);
    var result = voteInSessionUseCase.execute(vote, createVoteRequestDTO.cpf(), sessionId, createVoteRequestDTO.decision());
    var voteResponse = voteMapper.toVoteResponse(result);
    return ResponseEntity.status(HttpStatus.CREATED).body(voteResponse);
  }
}

