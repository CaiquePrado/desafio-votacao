package org.dbserver.desafiovotacao.v1.usecases.topic.impl;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.FindTopicByIdUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindTopicByIdUseCaseImpl implements FindTopicByIdUseCase {

  private final TopicRepository topicRepository;

  @Override
  public Topic find(UUID id) {
    return topicRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("No topic found."));
  }
}