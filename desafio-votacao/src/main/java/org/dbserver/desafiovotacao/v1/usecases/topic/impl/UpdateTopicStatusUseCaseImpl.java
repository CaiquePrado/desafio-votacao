package org.dbserver.desafiovotacao.v1.usecases.topic.impl;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.FindTopicByIdUseCase;
import org.dbserver.desafiovotacao.v1.usecases.topic.UpdateTopicStatusUseCase;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class UpdateTopicStatusUseCaseImpl implements UpdateTopicStatusUseCase {

  private final FindTopicByIdUseCase findTopicByIdUseCase;
  private final TopicRepository topicRepository;

  @Override
  public Topic execute(UUID topicId, TopicStatus topicStatus) {
    Topic foundTopic = findTopicByIdUseCase.find(topicId);
    foundTopic.setTopicStatus(topicStatus);
    return topicRepository.save(foundTopic);
  }
}
