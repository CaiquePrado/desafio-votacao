package org.dbserver.desafiovotacao.v1.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record CreateVotingSessionRequestDTO(

  @Schema(description = "Duration of the voting session in minutes", example = "1", requiredMode = RequiredMode.REQUIRED)
  @NotNull 
  @Min( value = 0, message = "Duration cannot be negative. If zero, the session duration will be 1 minute.")
  Integer duration
  
){}
