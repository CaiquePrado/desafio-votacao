package org.dbserver.desafiovotacao.v1.responses;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;

public record VoteResultResponseDTO(

  UUID votingSessionId,
  String firstName,
  String lastName,
  String cpf,
  VoteDecision decision

) {}
