package org.dbserver.desafiovotacao.v1.usecases.topic;

import java.util.List;

import org.dbserver.desafiovotacao.v1.models.Topic;

public interface FindTopicsByStatusUseCase {

  List<Topic> execute(String status);

  boolean isValidStatus(String status);

}
