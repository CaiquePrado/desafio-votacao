package org.dbserver.desafiovotacao.v1.requests;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

@Builder
public record CreateVoteRequestDTO(

  @Schema(example = "16822557008", requiredMode = RequiredMode.REQUIRED)
  @NotBlank(message = "CPF cannot be empty.")
  String cpf,

  @Schema(example = "YES", requiredMode = RequiredMode.REQUIRED)
  @NotNull(message = "VoteDecision cannot be null.")
  VoteDecision decision
  
) {}
