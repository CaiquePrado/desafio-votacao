package org.dbserver.desafiovotacao.v1.responses;

import java.util.UUID;

public record TopicResultResponseDTO(
  
  UUID id,
  String title,
  String description

) {}
