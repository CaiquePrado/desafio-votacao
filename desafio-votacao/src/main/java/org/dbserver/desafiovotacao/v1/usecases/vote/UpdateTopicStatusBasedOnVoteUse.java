package org.dbserver.desafiovotacao.v1.usecases.vote;

import java.util.UUID;

public interface UpdateTopicStatusBasedOnVoteUse {
  
  void execute(final UUID sessionId);

}
