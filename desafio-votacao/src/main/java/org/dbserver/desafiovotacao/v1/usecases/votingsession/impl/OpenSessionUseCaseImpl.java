package org.dbserver.desafiovotacao.v1.usecases.votingsession.impl;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VotingSessionRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.FindTopicByIdUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.OpenSessionUseCase;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class OpenSessionUseCaseImpl implements OpenSessionUseCase {

  private final VotingSessionRepository votingSessionRepository;
  private final FindTopicByIdUseCase findTopicByIdUseCase;

  @Override
  public VotingSession execute(VotingSession votingSession, UUID topicId, Integer duration) {
    Topic topic = findTopicByIdUseCase.find(topicId);
    votingSession.setTopic(topic);
    votingSession.setDuration(duration != null ? duration : 1);
    return votingSessionRepository.save(votingSession);
  }
}
