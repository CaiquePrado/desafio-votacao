package org.dbserver.desafiovotacao.v1.mapper;

import java.util.Map;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.requests.CreateVotingSessionRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.VoteResutResponseDTO;
import org.dbserver.desafiovotacao.v1.responses.VotingSessionResultResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface VotingSessionMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "closingTime", ignore = true)
  @Mapping(target = "createdAt", ignore = true)
  @Mapping(source = "createVotingSessionRequestDTO.duration", target = "duration")
  VotingSession toVotingSession(CreateVotingSessionRequestDTO createVotingSessionRequestDTO);

  @Mapping(source = "voteCounts", target = "voteCounts")
  @Mapping(source = "topicStatus", target = "topicStatus")
  VoteResutResponseDTO toVoteResult(Map<VoteDecision, Long> voteCounts, TopicStatus topicStatus);

  @Mapping(source = "topic.title", target = "topicTitle")
  @Mapping(source = "topic.topicStatus", target = "topicStatus")
  VotingSessionResultResponseDTO toVotingSessionResponse(VotingSession votingSession);

}
