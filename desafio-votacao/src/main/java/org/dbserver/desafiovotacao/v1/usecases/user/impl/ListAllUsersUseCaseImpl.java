package org.dbserver.desafiovotacao.v1.usecases.user.impl;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.ListAllUsersUseCase;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ListAllUsersUseCaseImpl implements ListAllUsersUseCase {

  private final UserRepository userRepository;

  @Override
  public Page<User> execute(int page, int size) {
    Pageable pageable = PageRequest.of(page, size);
    return userRepository.findByPage(pageable);
  }
}
