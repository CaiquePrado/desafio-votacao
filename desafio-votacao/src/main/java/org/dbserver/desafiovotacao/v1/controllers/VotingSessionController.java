package org.dbserver.desafiovotacao.v1.controllers;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.requests.CreateVotingSessionRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.VoteResutResponseDTO;
import org.dbserver.desafiovotacao.v1.responses.VotingSessionResultResponseDTO;
import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;

public interface VotingSessionController {

  @ApiResponses(value = {
                @ApiResponse(responseCode = "201", description = "Voting session created successfully", content = @Content(schema = @Schema(implementation = VotingSessionResultResponseDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "Open a voting session")
  ResponseEntity<VotingSessionResultResponseDTO> openVotingSession(
                @Valid CreateVotingSessionRequestDTO createVotingSessionRequestDTO,
                final UUID topicId);

  @ApiResponses(value = {
                @ApiResponse(responseCode = "200", description = "Voting result retrieved successfully", content = @Content(schema = @Schema(implementation = VoteResutResponseDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid request"),
                @ApiResponse(responseCode = "404", description = "Session not found") })
  @Operation(description = "Get voting result")
  ResponseEntity<VoteResutResponseDTO> getVotingResult(final UUID sessionId);
}
