package org.dbserver.desafiovotacao.v1.usecases.vote.impl;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.repository.VoteRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.VoteInSessionUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.VoteValidationUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.SessionStatusSchedulerUseCase;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class VoteInSessionUseCaseImpl implements VoteInSessionUseCase {

  private final VoteValidationUseCase voteValidationUseCase;
  private final FindUserByCpfUseCase findUserByCpfUseCase;
  private final FindSessionByIdUseCase findSessionByIdUseCase;
  private final SessionStatusSchedulerUseCase sessionStatusSchedulerUseCase;
  private final VoteRepository voteRepository;

  @Override
  public Vote execute(Vote vote, String cpf, UUID sessionId, VoteDecision decision) {

    voteValidationUseCase.validate(cpf, sessionId);

    var user = findUserByCpfUseCase.find(cpf);
    var session = findSessionByIdUseCase.find(sessionId);

    vote.setUser(user);
    vote.setVotingSession(session);
    vote.setDecision(decision);

    var savedVote = voteRepository.save(vote);
    if (LocalDateTime.now().isAfter(session.getClosingTime())) {
      sessionStatusSchedulerUseCase.updateSessionStatus(sessionId);
    }
    return savedVote;
  }
}
