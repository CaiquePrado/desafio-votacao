package org.dbserver.desafiovotacao.v1.enums;

public enum TopicStatus {
  REJECTED,
  APPROVED,
  STARTED,
  CLOSED,
  TIED
}
