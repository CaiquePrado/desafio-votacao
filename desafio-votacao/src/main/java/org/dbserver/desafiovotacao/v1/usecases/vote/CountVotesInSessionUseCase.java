package org.dbserver.desafiovotacao.v1.usecases.vote;

import java.util.Map;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;

public interface CountVotesInSessionUseCase {

  Map<VoteDecision, Long> execute(final UUID sessionId);

}
