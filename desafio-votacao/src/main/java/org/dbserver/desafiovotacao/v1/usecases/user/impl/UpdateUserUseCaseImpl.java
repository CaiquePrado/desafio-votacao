package org.dbserver.desafiovotacao.v1.usecases.user.impl;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.UpdateUserUseCase;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class UpdateUserUseCaseImpl implements UpdateUserUseCase {

  private final UserRepository userRepository;
  private final FindUserByCpfUseCase findUserByCpfUseCase;

  @Override
  public User execute(User user, String cpf) {
    var existingUser = findUserByCpfUseCase.find(cpf);
    existingUser.setFirstName(user.getFirstName());
    existingUser.setLastName(user.getLastName());
    existingUser.setAge(user.getAge());
    existingUser.setCpf(user.getCpf());
    return userRepository.save(existingUser);
  }
}
