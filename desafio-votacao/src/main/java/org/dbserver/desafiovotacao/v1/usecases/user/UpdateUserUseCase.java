package org.dbserver.desafiovotacao.v1.usecases.user;

import org.dbserver.desafiovotacao.v1.models.User;

public interface UpdateUserUseCase {

  User execute(User user, final String cpf);

}
