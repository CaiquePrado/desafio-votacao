package org.dbserver.desafiovotacao.v1.usecases.topic;

import org.dbserver.desafiovotacao.v1.models.Topic;

public interface CreateTopicUseCase {

  Topic execute(Topic topic);

}
