package org.dbserver.desafiovotacao.v1.usecases.vote.impl;

import java.util.Map;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.usecases.topic.UpdateTopicStatusUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.CountVotesInSessionUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.DetermineTopicStatusBasedInVoteUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.UpdateTopicStatusBasedOnVoteUse;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UpdateTopicStatusBasedOnVoteUseCaseImpl implements UpdateTopicStatusBasedOnVoteUse {

  private final CountVotesInSessionUseCase countVotesInSessionUseCase;
  private final DetermineTopicStatusBasedInVoteUseCase determineTopicStatusUseCase;
  private final UpdateTopicStatusUseCase updateTopicStatusUseCase;
  private final FindSessionByIdUseCase findSessionByIdUseCase;

  @Override
  public void execute(UUID sessionId) {
    Map<VoteDecision, Long> voteCounts = countVotesInSessionUseCase.execute(sessionId);
    TopicStatus newStatus = determineTopicStatusUseCase.execute(voteCounts);
    UUID topicId = findSessionByIdUseCase.find(sessionId).getTopic().getId();
    updateTopicStatusUseCase.execute(topicId, newStatus);
  }
}