package org.dbserver.desafiovotacao.v1.controllers.impl;

import java.util.List;

import org.dbserver.desafiovotacao.v1.controllers.TopicController;
import org.dbserver.desafiovotacao.v1.mapper.TopicMapper;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.requests.CreateTopicRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.TopicResultResponseDTO;
import org.dbserver.desafiovotacao.v1.usecases.topic.CreateTopicUseCase;
import org.dbserver.desafiovotacao.v1.usecases.topic.FindTopicsByStatusUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/topic")
@RequiredArgsConstructor
@Validated
@Tag(name = "Topic", description = "Endpoints for managing Topics.")
public class TopicControllerImpl implements TopicController {

  private final CreateTopicUseCase createTopicUseCase;
  private final FindTopicsByStatusUseCase findTopicsByStatusUseCase;
  private final TopicMapper topicMapper;

  @Override
  @PostMapping
  public ResponseEntity<TopicResultResponseDTO> createTopic(@RequestBody @Valid CreateTopicRequestDTO createTopicRequestDTO) {
    var topic = topicMapper.toTopic(createTopicRequestDTO);
    var result = createTopicUseCase.execute(topic);
    var topicResponse = topicMapper.toTopicResponse(result);
    return ResponseEntity.status(HttpStatus.CREATED).body(topicResponse);
  }

  @Override
  @GetMapping("/{status}")
  public ResponseEntity<List<Topic>> findTopicsByStatus(
      @Parameter(description = "The status of the topic", schema = @Schema(allowableValues = { "Rejected", "Approved",
          "Started", "Closed" })) @PathVariable String status) {
    List<Topic> topics = findTopicsByStatusUseCase.execute(status);
    return ResponseEntity.ok(topics);
  }
}
