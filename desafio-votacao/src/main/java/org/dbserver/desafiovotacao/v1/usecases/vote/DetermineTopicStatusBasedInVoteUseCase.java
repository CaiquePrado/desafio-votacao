package org.dbserver.desafiovotacao.v1.usecases.vote;

import java.util.Map;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;

public interface DetermineTopicStatusBasedInVoteUseCase {
  
  TopicStatus execute(Map<VoteDecision, Long> voteCounts);

}
