package org.dbserver.desafiovotacao.v1.usecases.votingsession.impl;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VotingSessionRepository;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindSessionByIdUseCaseImpl implements FindSessionByIdUseCase {

  private final VotingSessionRepository votingSessionRepository;

  @Override
  public VotingSession find(UUID id) {
    return votingSessionRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("No session found."));
  }
}
