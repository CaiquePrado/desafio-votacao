package org.dbserver.desafiovotacao.v1.controllers;

import java.util.List;

import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.requests.CreateTopicRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.TopicResultResponseDTO;
import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;

public interface TopicController {

  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Topic created successfully", content = @Content(schema = @Schema(implementation = TopicResultResponseDTO.class))),
      @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "Create a topic")
  ResponseEntity<TopicResultResponseDTO> createTopic(@Valid CreateTopicRequestDTO createTopicRequestDTO);

  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Topics found successfully", content = @Content(schema = @Schema(implementation = Topic.class))),
      @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "Find topics by status")
  ResponseEntity<List<Topic>> findTopicsByStatus(String status);

}
