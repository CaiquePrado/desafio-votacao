package org.dbserver.desafiovotacao.v1.repository;

import java.util.List;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, UUID> {

  @Query("SELECT t FROM Topic t WHERE UPPER(t.topicStatus) = UPPER(:status)")
  List<Topic> findByTopicStatusIgnoreCase(@Param("status") String status);

}
