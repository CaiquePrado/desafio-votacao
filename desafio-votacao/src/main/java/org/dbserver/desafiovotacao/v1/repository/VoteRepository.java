package org.dbserver.desafiovotacao.v1.repository;

import java.util.List;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote, UUID> {

  @Query("SELECT COUNT(v) > 0 FROM Vote v WHERE v.user = :user AND v.votingSession = :votingSession")
  boolean hasUserVotedInSession(@Param("user") User user, @Param("votingSession") VotingSession votingSession);

  List<Vote> findByVotingSession(VotingSession session);

}
