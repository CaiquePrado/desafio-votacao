package org.dbserver.desafiovotacao.v1.requests;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Builder;

@Builder
public record CreateTopicRequestDTO(
  
  @Schema(example = "Lorem ipsum dolor sit amet", requiredMode = RequiredMode.REQUIRED)
  @NotBlank(message = "Title cannot be empty.")
  @Size(max = 100, message = "Title must be at most 100 characters.")
  String title,

  @Schema(example = "Lorem ipsum dolor sit amet", requiredMode = RequiredMode.REQUIRED)
  @NotBlank(message = "Description cannot be empty.")
  String description

) {}
