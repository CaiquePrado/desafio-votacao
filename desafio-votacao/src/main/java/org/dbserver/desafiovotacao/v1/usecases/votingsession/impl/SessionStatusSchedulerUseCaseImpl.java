package org.dbserver.desafiovotacao.v1.usecases.votingsession.impl;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.usecases.vote.UpdateTopicStatusBasedOnVoteUse;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.SessionStatusSchedulerUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SessionStatusSchedulerUseCaseImpl implements SessionStatusSchedulerUseCase {

  private final UpdateTopicStatusBasedOnVoteUse updateTopicStatusBasedOnVoteUse;
  private final FindSessionByIdUseCase findSessionByIdUseCase;

  public void updateSessionStatus(UUID sessionId) {
    var session = findSessionByIdUseCase.find(sessionId);

    if (LocalDateTime.now().isAfter(session.getClosingTime())) {
      updateTopicStatusBasedOnVoteUse.execute(session.getId());
    }
  }
}
