package org.dbserver.desafiovotacao.v1.usecases.vote.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VoteRepository;
import org.dbserver.desafiovotacao.v1.usecases.vote.CountVotesInSessionUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CountVotesInSessionUseCaseImpl implements CountVotesInSessionUseCase {

  private final VoteRepository voteRepository;
  private final FindSessionByIdUseCase findSessionByIdUseCase;

  @Override
  public Map<VoteDecision, Long> execute(UUID sessionId) {

    VotingSession session = findSessionByIdUseCase.find(sessionId);
    List<Vote> votes = voteRepository.findByVotingSession(session);

    return votes.stream()
        .collect(Collectors.groupingBy(Vote::getDecision, Collectors.counting()));
  }
}
