package org.dbserver.desafiovotacao.v1.usecases.votingsession;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.VotingSession;

public interface OpenSessionUseCase {

  VotingSession execute(VotingSession votingSession, final UUID topicId, Integer duration);

}
