package org.dbserver.desafiovotacao.v1.usecases.user.impl;

import org.dbserver.desafiovotacao.v1.exceptions.InvalidRequestException;
import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindUserByCpfUseCaseImpl implements FindUserByCpfUseCase {

  private final UserRepository userRepository;

  @Override
  public User find(String cpf) {
    return userRepository.findByCpf(cpf)
        .orElseThrow(() -> new ResourceNotFoundException("No user found."));
  }

  @Override
  public void checkIfCpfExists(String cpf) {
    userRepository.findByCpf(cpf).ifPresent(student -> {
      throw new InvalidRequestException("A user with these CPF already exists.");
    });
  }
}
