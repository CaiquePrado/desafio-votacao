package org.dbserver.desafiovotacao.v1.usecases.topic.impl;

import java.util.List;
import java.util.stream.Stream;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.exceptions.InvalidStatusException;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.FindTopicsByStatusUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindTopicsByStatusUseCaseImpl implements FindTopicsByStatusUseCase {

  private final TopicRepository topicRepository;

  @Override
  public List<Topic> execute(String status) {
    if (!isValidStatus(status))
      throw new InvalidStatusException(status);
    return topicRepository.findByTopicStatusIgnoreCase(status);
  }

  public boolean isValidStatus(String status) {
    return Stream.of(TopicStatus.values())
        .anyMatch(topicStatus -> topicStatus.name().equalsIgnoreCase(status));
  }
}
