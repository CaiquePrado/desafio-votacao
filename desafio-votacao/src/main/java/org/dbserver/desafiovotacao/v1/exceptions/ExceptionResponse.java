package org.dbserver.desafiovotacao.v1.exceptions;
import java.time.LocalDateTime;

public record ExceptionResponse(
  String message,
	LocalDateTime timestamp
) {}
