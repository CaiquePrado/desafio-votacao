package org.dbserver.desafiovotacao.v1.mapper;

import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.requests.CreateVoteRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.VoteResultResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface VoteMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "createdAt", ignore = true)
  @Mapping(source = "createVoteRequestDTO.cpf", target = "user.cpf")
  @Mapping(source = "createVoteRequestDTO.decision", target = "decision")
  Vote toVote(CreateVoteRequestDTO createVoteRequestDTO);

  @Mapping(source = "votingSession.id", target = "votingSessionId")
  @Mapping(source = "user.firstName", target = "firstName")
  @Mapping(source = "user.lastName", target = "lastName")
  @Mapping(source = "user.cpf", target = "cpf")
  VoteResultResponseDTO toVoteResponse(Vote vote);

}
