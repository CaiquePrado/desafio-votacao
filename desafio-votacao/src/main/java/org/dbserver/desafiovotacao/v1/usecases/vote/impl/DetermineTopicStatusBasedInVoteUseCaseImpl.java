package org.dbserver.desafiovotacao.v1.usecases.vote.impl;

import java.util.Map;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.usecases.vote.DetermineTopicStatusBasedInVoteUseCase;
import org.springframework.stereotype.Service;

@Service
public class DetermineTopicStatusBasedInVoteUseCaseImpl implements DetermineTopicStatusBasedInVoteUseCase{
  
  @Override
  public TopicStatus execute(Map<VoteDecision, Long> voteCounts) {
    long yesVotes = voteCounts.getOrDefault(VoteDecision.YES, 0L);
    long noVotes = voteCounts.getOrDefault(VoteDecision.NO, 0L);

    if (yesVotes > noVotes) {
      return TopicStatus.APPROVED;
    } else if (noVotes > yesVotes) {
      return TopicStatus.REJECTED;
    } else {
      return TopicStatus.TIED;
    }
  }
}
