package org.dbserver.desafiovotacao.v1.usecases.user;

import org.dbserver.desafiovotacao.v1.models.User;

public interface FindUserByCpfUseCase {

  User find(final String cpf);

  void checkIfCpfExists(final String cpf);

}
