package org.dbserver.desafiovotacao.v1.models;

import java.time.LocalDateTime;
import java.util.UUID;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tb_session")
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Data
public class VotingSession {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  private LocalDateTime openingTime;

  private LocalDateTime closingTime;

  @OneToOne
  @JoinColumn(name = "topic_id")
  @JsonManagedReference
  private Topic topic;

  @Builder.Default
  private Integer duration = 1;

  @CreationTimestamp
  private LocalDateTime createdAt;

  @PrePersist
  protected void onCreate() {
    openingTime = LocalDateTime.now();
    closingTime = openingTime.plusMinutes(duration);
  }
}
