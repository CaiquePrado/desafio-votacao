package org.dbserver.desafiovotacao.v1.controllers;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.requests.CreateorUpdateuserRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;

public interface UserController {

  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "User created successfully", content = @Content(schema = @Schema(implementation = User.class))),
      @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "Create a user")
  ResponseEntity<User> createUser(@Valid CreateorUpdateuserRequestDTO userDTO);

  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "User updated successfully", content = @Content(schema = @Schema(implementation = CreateorUpdateuserRequestDTO.class))),
      @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "Update a user")
  ResponseEntity<User> updateUser(@Valid CreateorUpdateuserRequestDTO userDTO,
      final String cpf);

  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "User deleted successfully"),
      @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "Delete a user")
  ResponseEntity<String> deleteUser(final String cpf);

  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Users retrieved successfully", content = @Content(schema = @Schema(implementation = User.class))),
      @ApiResponse(responseCode = "400", description = "Invalid request") })
  @Operation(description = "List all users")
  ResponseEntity<Page<User>> listAllUsers(int page, int size);

  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "User retrieved successfully", content = @Content(schema = @Schema(implementation = User.class))),
      @ApiResponse(responseCode = "400", description = "Invalid request"),
      @ApiResponse(responseCode = "404", description = "User not found")
  })
  @Operation(description = "Find a user by CPF")
  ResponseEntity<User> findOneUser(final String cpf);

}
