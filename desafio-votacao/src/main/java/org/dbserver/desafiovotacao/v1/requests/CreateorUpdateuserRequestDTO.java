package org.dbserver.desafiovotacao.v1.requests;

import org.hibernate.validator.constraints.br.CPF;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;

@Builder
public record CreateorUpdateuserRequestDTO(

  @Schema(example = "Fulano", requiredMode= RequiredMode.REQUIRED)
  @NotBlank(message = "First name cannot be empty.")
  @Size(max = 100, message = "Name must be at most 100 characters.")
  String firstName,

  @Schema(example = "Tal", requiredMode= RequiredMode.REQUIRED)
  @NotBlank(message = "Last name cannot be empty.")
  @Size(max = 100, message = "Name must be at most 100 characters.")
  String lastName,

  @Schema(example = "20", requiredMode= RequiredMode.REQUIRED)
  @NotNull(message = "Age cannot be null.")
  @Min(value = 18, message = "Age must be at least 18.")
  Integer age,

  @Schema(example = "16822557008", requiredMode= RequiredMode.REQUIRED)
  @NotBlank(message = "CPF cannot be empty.")
  @CPF(message = "CPF format is invalid, it should be 00000000000 or 000.000.000-00")
  String cpf

) {}
