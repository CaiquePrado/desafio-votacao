package org.dbserver.desafiovotacao.v1.repository;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotingSessionRepository extends JpaRepository<VotingSession, UUID> {

}
