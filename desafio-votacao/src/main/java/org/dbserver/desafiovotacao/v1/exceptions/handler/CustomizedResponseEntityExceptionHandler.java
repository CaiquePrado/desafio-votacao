package org.dbserver.desafiovotacao.v1.exceptions.handler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.dbserver.desafiovotacao.v1.exceptions.ExceptionResponse;
import org.dbserver.desafiovotacao.v1.exceptions.InvalidRequestException;
import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler {

  @ExceptionHandler(RuntimeException.class)
  public final ResponseEntity<ExceptionResponse> handleUnexpectedErrors(RuntimeException unexpectedError,
      WebRequest request) {
    return createErrorEntity("Unexpected error: " + unexpectedError.getMessage(),
        HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public final ResponseEntity<ExceptionResponse> handleConstraintViolation(MethodArgumentNotValidException e) {
    List<String> errorDetails = new ArrayList<>();
    e.getBindingResult().getAllErrors().stream()
        .forEach(error -> errorDetails.add(error.getDefaultMessage()));
    return createErrorEntity("Please fill in the data correctly " + errorDetails,
        HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  public final ResponseEntity<ExceptionResponse> handleNotFoundExceptions(ResourceNotFoundException ex) {
    return createErrorEntity(ex.getMessage(), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(InvalidRequestException.class)
  public final ResponseEntity<ExceptionResponse> handleInvalidRequestException(InvalidRequestException ex) {
    return createErrorEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
  }

  private ResponseEntity<ExceptionResponse> createErrorEntity(String message, HttpStatus statusCode) {
    ExceptionResponse errorBody = new ExceptionResponse(message, LocalDateTime.now());
    log.info("Error: ", errorBody);
    return new ResponseEntity<>(errorBody, statusCode);
  }
}
