package org.dbserver.desafiovotacao.v1.usecases.topic.impl;

import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.CreateTopicUseCase;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class CreateTopicUseCaseImpl implements CreateTopicUseCase {

  private final TopicRepository topicRepository;

  @Override
  public Topic execute(Topic topic) {
    return topicRepository.save(topic);
  }
}
