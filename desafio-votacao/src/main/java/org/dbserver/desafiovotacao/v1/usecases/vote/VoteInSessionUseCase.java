package org.dbserver.desafiovotacao.v1.usecases.vote;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.Vote;

public interface VoteInSessionUseCase {

  Vote execute(Vote vote, final String cpf, final UUID sessionId, VoteDecision decision);

}
