package org.dbserver.desafiovotacao.v1.usecases.vote.impl;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.exceptions.InvalidRequestException;
import org.dbserver.desafiovotacao.v1.repository.VoteRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.VoteValidationUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class VoteValidationUseCaseImpl implements VoteValidationUseCase {

  private final FindUserByCpfUseCase findUserByCpfUseCase;
  private final FindSessionByIdUseCase findSessionByIdUseCase;
  private final VoteRepository voteRepository;

  public void validate(String cpf, UUID sessionId) {
    var user = findUserByCpfUseCase.find(cpf);
    var session = findSessionByIdUseCase.find(sessionId);

    if (voteRepository.hasUserVotedInSession(user, session))
      throw new InvalidRequestException("User has already voted in this session");

    if (LocalDateTime.now().isAfter(session.getClosingTime()))
      throw new InvalidRequestException("Voting session has already ended");
  }
}
