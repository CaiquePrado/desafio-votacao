package org.dbserver.desafiovotacao.v1.usecases.user;

import org.dbserver.desafiovotacao.v1.models.User;
import org.springframework.data.domain.Page;

public interface ListAllUsersUseCase {

  Page<User> execute(int page, int size);

}
