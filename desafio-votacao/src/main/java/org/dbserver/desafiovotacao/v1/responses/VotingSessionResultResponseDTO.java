package org.dbserver.desafiovotacao.v1.responses;

import java.util.UUID;

public record VotingSessionResultResponseDTO(
  
  UUID id,
  String topicTitle,
  String topicStatus,
  Integer duration

){}
