package org.dbserver.desafiovotacao.v1.usecases.votingsession;

import java.util.UUID;

public interface SessionStatusSchedulerUseCase {

  void updateSessionStatus(final UUID sessionId);

}
