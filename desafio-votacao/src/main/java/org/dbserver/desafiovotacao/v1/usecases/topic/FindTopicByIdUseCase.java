package org.dbserver.desafiovotacao.v1.usecases.topic;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.Topic;

public interface FindTopicByIdUseCase {
  
  Topic find(final UUID id);

}
