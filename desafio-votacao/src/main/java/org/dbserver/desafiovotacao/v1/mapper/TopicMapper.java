package org.dbserver.desafiovotacao.v1.mapper;

import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.requests.CreateTopicRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.TopicResultResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TopicMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "topicStatus", ignore = true)
  @Mapping(target = "createdAt", ignore = true)
  @Mapping(target = "votingSession", ignore = true)
  Topic toTopic(CreateTopicRequestDTO createTopicRequestDTO);

  TopicResultResponseDTO toTopicResponse(Topic topic);

}
