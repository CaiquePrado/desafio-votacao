package org.dbserver.desafiovotacao.v1.mapper;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.requests.CreateorUpdateuserRequestDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "createdAt", ignore = true)
  User toUser(CreateorUpdateuserRequestDTO createorUpdateuserRequestDTO);

}
