package org.dbserver.desafiovotacao.v1.controllers.impl;

import org.dbserver.desafiovotacao.v1.controllers.UserController;
import org.dbserver.desafiovotacao.v1.mapper.UserMapper;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.requests.CreateorUpdateuserRequestDTO;
import org.dbserver.desafiovotacao.v1.usecases.user.CreateUserUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.DeleteUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.ListAllUsersUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.UpdateUserUseCase;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Validated
@Tag(name = "User", description = "Endpoints for managing Users.")
public class UserControllerImpl implements UserController {

  private final CreateUserUseCase createUserUseCase;
  private final UpdateUserUseCase updateUserUseCase;
  private final DeleteUserByCpfUseCase deleteUserByCpfUseCase;
  private final ListAllUsersUseCase listAllUsersUseCase;
  private final FindUserByCpfUseCase findUserByCpfUseCase;
  private final UserMapper userMapper;

  @PostMapping
  @Override
  public ResponseEntity<User> createUser(@RequestBody @Valid CreateorUpdateuserRequestDTO userDTO) {
    var user = userMapper.toUser(userDTO);
    var result = createUserUseCase.execute(user);
    return ResponseEntity.status(HttpStatus.CREATED).body(result);
  }

  @PutMapping("/{cpf}")
  @Override
  public ResponseEntity<User> updateUser(@RequestBody @Valid CreateorUpdateuserRequestDTO userDTO,
      @PathVariable String cpf) {
    var user = userMapper.toUser(userDTO);
    var result = updateUserUseCase.execute(user, cpf);
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }

  @DeleteMapping("/{cpf}")
  @Override
  public ResponseEntity<String> deleteUser(@PathVariable String cpf) {
    deleteUserByCpfUseCase.execute(cpf);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).body("User deleted successfully.");
  }

  @GetMapping
  @Override
  public ResponseEntity<Page<User>> listAllUsers(@RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "10") int size) {
    Page<User> result = listAllUsersUseCase.execute(page, size);
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }

  @GetMapping("/{cpf}")
  @Override
  public ResponseEntity<User> findOneUser(@PathVariable String cpf) {
    var result = findUserByCpfUseCase.find(cpf);
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }
}
