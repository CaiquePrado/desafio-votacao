package org.dbserver.desafiovotacao.v1.usecases.user;

public interface DeleteUserByCpfUseCase {

  void execute(final String cpf);

}
