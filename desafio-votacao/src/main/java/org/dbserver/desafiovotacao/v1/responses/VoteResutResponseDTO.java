package org.dbserver.desafiovotacao.v1.responses;

import java.util.Map;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;

public record VoteResutResponseDTO(

  @Schema(example = "{YES=10, NO=5}", requiredMode = RequiredMode.REQUIRED)
  Map<VoteDecision, Long> voteCounts,

  @Schema(example = "Approved", requiredMode = RequiredMode.REQUIRED)
  TopicStatus topicStatus

) {}
