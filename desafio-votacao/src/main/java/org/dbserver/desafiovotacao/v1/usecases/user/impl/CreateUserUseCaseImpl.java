package org.dbserver.desafiovotacao.v1.usecases.user.impl;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.CreateUserUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class CreateUserUseCaseImpl implements CreateUserUseCase {

  private final UserRepository userRepository;
  private final FindUserByCpfUseCase findUserByCpfUseCase;

  @Override
  public User execute(User user) {
    findUserByCpfUseCase.checkIfCpfExists(user.getCpf());
    return userRepository.save(user);
  }
}
