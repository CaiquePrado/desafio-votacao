package org.dbserver.desafiovotacao.v1.usecases.user;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.models.User;

public interface FindUserByIdUseCase {

  User find(final UUID id);

}
