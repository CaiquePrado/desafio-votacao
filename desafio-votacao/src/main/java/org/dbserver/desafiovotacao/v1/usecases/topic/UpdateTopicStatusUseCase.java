package org.dbserver.desafiovotacao.v1.usecases.topic;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.models.Topic;

public interface UpdateTopicStatusUseCase {

  Topic execute(final UUID id, TopicStatus topicStatus);

}
