package org.dbserver.desafiovotacao.v1.controllers.impl;

import java.util.Map;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.controllers.VotingSessionController;
import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.mapper.VotingSessionMapper;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.requests.CreateVotingSessionRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.VoteResutResponseDTO;
import org.dbserver.desafiovotacao.v1.responses.VotingSessionResultResponseDTO;
import org.dbserver.desafiovotacao.v1.usecases.vote.CountVotesInSessionUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.OpenSessionUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.SessionStatusSchedulerUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/votingsession")
@RequiredArgsConstructor
@Validated
@Tag(name = "Voting Session", description = "Endpoints for managing voting Sessions.")
public class VotingSessionControllerImpl implements VotingSessionController {

  private final OpenSessionUseCase openSessionUseCase;
  private final FindSessionByIdUseCase findSessionByIdUseCase;
  private final CountVotesInSessionUseCase countVotesInSessionUseCase;
  private final SessionStatusSchedulerUseCase sessionStatusSchedulerUseCase;
  private final VotingSessionMapper votingSessionMapper;

  @Override
  @PostMapping("/{topicId}")
  public ResponseEntity<VotingSessionResultResponseDTO> openVotingSession(
      @RequestBody @Valid CreateVotingSessionRequestDTO createVotingSessionRequestDTO, @PathVariable UUID topicId) {
    var votingSession = votingSessionMapper.toVotingSession(createVotingSessionRequestDTO);
    Integer duration = createVotingSessionRequestDTO.duration();
    var result = openSessionUseCase.execute(votingSession, topicId, duration);
    var votingSessionResponse = votingSessionMapper.toVotingSessionResponse(result);
    return ResponseEntity.status(HttpStatus.CREATED).body(votingSessionResponse);
  }

  @Override
  @GetMapping("/{sessionId}/result")
  public ResponseEntity<VoteResutResponseDTO> getVotingResult(@PathVariable UUID sessionId) {
    sessionStatusSchedulerUseCase.updateSessionStatus(sessionId);
    Map<VoteDecision, Long> voteCounts = countVotesInSessionUseCase.execute(sessionId);
    TopicStatus topicStatus = findSessionByIdUseCase.find(sessionId).getTopic().getTopicStatus();
    VoteResutResponseDTO result = votingSessionMapper.toVoteResult(voteCounts, topicStatus);
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }
}
