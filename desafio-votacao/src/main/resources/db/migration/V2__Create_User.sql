CREATE TABLE tb_user (
    id UUID PRIMARY KEY,
    first_name VARCHAR(100),
    last_name VARCHAR(100),
    age INTEGER,
    cpf VARCHAR(14) UNIQUE,
    created_at TIMESTAMP
);