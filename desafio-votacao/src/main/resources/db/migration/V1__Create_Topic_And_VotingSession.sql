CREATE TABLE tb_topic (
    id UUID PRIMARY KEY,
    title VARCHAR(255),
    description VARCHAR(255),
    topic_status VARCHAR(255),
    created_at TIMESTAMP,
    voting_session_id UUID
);

CREATE TABLE tb_session (
    id UUID PRIMARY KEY,
    opening_time TIMESTAMP,
    closing_time TIMESTAMP,
    created_at TIMESTAMP,
    topic_id UUID
);

ALTER TABLE tb_session ADD CONSTRAINT fk_session_topic FOREIGN KEY (topic_id) REFERENCES tb_topic(id);