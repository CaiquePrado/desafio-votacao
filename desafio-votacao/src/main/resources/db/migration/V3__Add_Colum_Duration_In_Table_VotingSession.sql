ALTER TABLE tb_session
ADD COLUMN duration INTEGER DEFAULT 1;

ALTER TABLE tb_topic
ADD COLUMN session_id UUID;

ALTER TABLE tb_topic
ADD CONSTRAINT fk_topic_session FOREIGN KEY (session_id) REFERENCES tb_session(id);