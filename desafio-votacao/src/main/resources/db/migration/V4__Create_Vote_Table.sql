CREATE TABLE tb_vote (
    id UUID PRIMARY KEY,
    decision VARCHAR(3),
    created_at TIMESTAMP,
    user_id UUID,
    session_id UUID
);

ALTER TABLE tb_vote ADD CONSTRAINT fk_vote_user FOREIGN KEY (user_id) REFERENCES tb_user(id);
ALTER TABLE tb_vote ADD CONSTRAINT fk_vote_session FOREIGN KEY (session_id) REFERENCES tb_session(id);