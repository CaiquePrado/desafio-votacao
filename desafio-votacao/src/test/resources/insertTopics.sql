INSERT INTO tb_topic (id, title, description, topic_status, created_at) VALUES
('f1eebc99-9c0b-4ef8-bb6d-6bb9bd380a16', 'Topic 1', 'Description for Topic 1', 'STARTED', '2024-04-01 14:52:00.000000'),
('f1eebc99-9c0b-4ef8-bb6d-6bb9bd380a17', 'Topic 2', 'Description for Topic 2', 'CLOSED', '2024-04-01 14:52:10.000000'),
('f1eebc99-9c0b-4ef8-bb6d-6bb9bd380a18', 'Topic 3', 'Description for Topic 3', 'REJECTED', '2024-04-01 14:52:20.000000'),
('f1eebc99-9c0b-4ef8-bb6d-6bb9bd380a19', 'Topic 4', 'Description for Topic 4', 'APPROVED', '2024-04-01 14:52:30.000000'),
('f1eebc99-9c0b-4ef8-bb6d-6bb9bd380a20', 'Topic 5', 'Description for Topic 5', 'TIED', '2024-04-01 14:52:40.000000');
