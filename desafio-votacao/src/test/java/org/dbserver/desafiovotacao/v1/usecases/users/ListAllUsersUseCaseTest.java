package org.dbserver.desafiovotacao.v1.usecases.users;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.impl.ListAllUsersUseCaseImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class ListAllUsersUseCaseTest {

  @Mock
  UserRepository userRepository;

  @InjectMocks
  ListAllUsersUseCaseImpl listAllUsersUseCaseImpl;

  User user;
  Page<User> usersPage;

  @BeforeEach
  void setup() {
    user = User.builder()
        .firstName("Jonas")
        .lastName("Machado")
        .age(33)
        .cpf("90238124096")
        .build();

    List<User> usersList = Arrays.asList(user);
    usersPage = new PageImpl<>(usersList);

    given(userRepository.findByPage(any(Pageable.class))).willReturn(usersPage);
  }

  @DisplayName("Given Page and Size when List All Users should Return Page of Users")
  @Test
  void shouldListAllUsersAndReturnPageOfUsers() {

    Page<User> foundUsersPage = listAllUsersUseCaseImpl.execute(0, 1);

    assertNotNull(foundUsersPage);
    assertEquals(usersPage.getTotalElements(), foundUsersPage.getTotalElements());
    assertEquals(usersPage.getContent().get(0).getFirstName(), foundUsersPage.getContent().get(0).getFirstName());
    assertEquals(usersPage.getContent().get(0).getLastName(), foundUsersPage.getContent().get(0).getLastName());
    assertEquals(usersPage.getContent().get(0).getAge(), foundUsersPage.getContent().get(0).getAge());
    assertEquals(usersPage.getContent().get(0).getCpf(), foundUsersPage.getContent().get(0).getCpf());
  }
}
