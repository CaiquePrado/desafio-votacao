package org.dbserver.desafiovotacao.v1.utils.stubs;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.requests.CreateorUpdateuserRequestDTO;

public class UserEntitiesBuilder {

  public static CreateorUpdateuserRequestDTO validCreateUserStub() {
    return CreateorUpdateuserRequestDTO.builder()
        .firstName("Lorem")
        .lastName("ipsum")
        .age(21)
        .cpf("52906411060")
        .build();
  }

  public static CreateorUpdateuserRequestDTO invalidCreateUserStub() {
    return CreateorUpdateuserRequestDTO.builder()
        .firstName("")
        .lastName("")
        .age(0)
        .cpf("")
        .build();
  }

  public static CreateorUpdateuserRequestDTO validUpdateUserStub() {
    return CreateorUpdateuserRequestDTO.builder()
        .firstName("Carlos")
        .lastName("Magno")
        .age(60)
        .cpf("74300697035")
        .build();
  }

  public static User validUser() {
    return User.builder()
        .firstName("Jonas")
        .lastName("Machado")
        .age(33)
        .cpf("90238124096")
        .build();
  }

  public static User updatedUser() {
    return User.builder()
        .firstName("Maria")
        .lastName("Silva")
        .age(30)
        .cpf("90238124096")
        .build();
  }

}
