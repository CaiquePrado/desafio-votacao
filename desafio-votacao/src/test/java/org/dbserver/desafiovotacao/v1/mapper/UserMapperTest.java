package org.dbserver.desafiovotacao.v1.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.requests.CreateorUpdateuserRequestDTO;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

class UserMapperTest {

  UserMapper userMapper = Mappers.getMapper(UserMapper.class);

  @DisplayName("Given CreateorUpdateuserRequestDTO Object When Map Then Return User")
  @Test
  void shouldMapCreateorUpdateuserRequestDTOToUser() {

    CreateorUpdateuserRequestDTO createorUpdateuserRequestDTO = UserEntitiesBuilder.validCreateUserStub();

    User user = userMapper.toUser(createorUpdateuserRequestDTO);

    assertNotNull(user);
    assertEquals(createorUpdateuserRequestDTO.firstName(), user.getFirstName());
    assertEquals(createorUpdateuserRequestDTO.lastName(), user.getLastName());
    assertEquals(createorUpdateuserRequestDTO.age(), user.getAge());
    assertEquals(createorUpdateuserRequestDTO.cpf(), user.getCpf());
  }
}
