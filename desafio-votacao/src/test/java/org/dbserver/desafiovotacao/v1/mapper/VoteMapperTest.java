package org.dbserver.desafiovotacao.v1.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.requests.CreateVoteRequestDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

class VoteMapperTest {

  VoteMapper voteMapper = Mappers.getMapper(VoteMapper.class);

  CreateVoteRequestDTO createVoteRequestDTO = CreateVoteRequestDTO.builder()
      .cpf("16822557008")
      .decision(VoteDecision.YES)
      .build();

  @DisplayName("Given CreateVoteRequestDTO Object When Map Then Return Vote")
  @Test
  void shouldMapCreateVoteRequestDTOToVote() {

    Vote vote = voteMapper.toVote(createVoteRequestDTO);

    assertNotNull(vote);
    assertNull(vote.getId());
    assertNull(vote.getCreatedAt());
    assertEquals(createVoteRequestDTO.cpf(), vote.getUser().getCpf());
    assertEquals(createVoteRequestDTO.decision(), vote.getDecision());
  }
}
