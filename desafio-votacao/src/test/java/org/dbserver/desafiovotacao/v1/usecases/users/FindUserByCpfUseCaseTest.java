package org.dbserver.desafiovotacao.v1.usecases.users;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import java.util.Optional;

import org.dbserver.desafiovotacao.v1.exceptions.InvalidRequestException;
import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.impl.FindUserByCpfUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindUserByCpfUseCaseTest {

  @Mock
  UserRepository userRepository;

  @InjectMocks
  FindUserByCpfUseCaseImpl findUserByCpfUseCaseImpl;

  User user = UserEntitiesBuilder.validUser();

  @DisplayName("Given User CPF when Find User should Return User")
  @Test
  void shouldFindUserByCpfAndReturnUser() {

    given(userRepository.findByCpf(anyString())).willReturn(Optional.of(user));

    User foundUser = findUserByCpfUseCaseImpl.find(user.getCpf());

    assertNotNull(foundUser);
    assertEquals(user.getFirstName(), foundUser.getFirstName());
    assertEquals(user.getLastName(), foundUser.getLastName());
    assertEquals(user.getAge(), foundUser.getAge());
    assertEquals(user.getCpf(), foundUser.getCpf());
  }

  @DisplayName("Given Non-Existing User CPF when Find User should Throw ResourceNotFoundException")
  @Test
  void shouldThrowResourceNotFoundExceptionWhenFindNonExistingUserByCpf() {

    given(userRepository.findByCpf(anyString())).willReturn(Optional.empty());

    ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> {
      findUserByCpfUseCaseImpl.find(user.getCpf());
    });

    assertEquals("No user found.", result.getMessage());
  }

  @DisplayName("Given Existing User CPF when Check If CPF Exists should Throw InvalidRequestException")
  @Test
  void shouldThrowInvalidRequestExceptionWhenCheckExistingUserCpf() {

    given(userRepository.findByCpf(anyString())).willReturn(Optional.of(user));

    InvalidRequestException result = assertThrows(InvalidRequestException.class, () -> {
      findUserByCpfUseCaseImpl.checkIfCpfExists(user.getCpf());
    });

    assertEquals("A user with these CPF already exists.", result.getMessage());
  }

  @DisplayName("Given Non-Existing User CPF when Check If CPF Exists should Execute Without Exception")
  @Test
  void shouldExecuteWithoutExceptionWhenCheckNonExistingUserCpf() {

    given(userRepository.findByCpf(anyString())).willReturn(Optional.empty());

    assertDoesNotThrow(() -> {
      findUserByCpfUseCaseImpl.checkIfCpfExists(user.getCpf());
    });
  }
}