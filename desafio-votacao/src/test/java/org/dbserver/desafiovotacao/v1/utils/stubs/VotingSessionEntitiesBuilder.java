package org.dbserver.desafiovotacao.v1.utils.stubs;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.requests.CreateVotingSessionRequestDTO;

public class VotingSessionEntitiesBuilder {

  public static CreateVotingSessionRequestDTO validCreateVotingSessioncStub() {
    return CreateVotingSessionRequestDTO.builder()
        .duration(1)
        .build();
  }

  public static VotingSession validVotingSession(Topic topic) {
    return validVotingSessionWithId(UUID.randomUUID(), topic);
  }

  public static VotingSession validVotingSessionWithId(UUID id, Topic topic) {
    return VotingSession.builder()
        .id(id)
        .openingTime(LocalDateTime.now())
        .closingTime(LocalDateTime.now().plusMinutes(1))
        .topic(topic)
        .duration(1)
        .createdAt(LocalDateTime.now())
        .build();
  }

  public static VotingSession validVotingSessionWithStatus(TopicStatus status) {
    Topic topic = TopicEntitiesBuiler.validTopicStatus(status);
    return validVotingSession(topic);
  }

  public static VotingSession stubVotingSession(UUID id, Topic topic) {
    return VotingSession.builder()
        .id(id)
        .openingTime(LocalDateTime.now())
        .closingTime(LocalDateTime.now().plusMinutes(1))
        .topic(topic)
        .duration(1)
        .createdAt(LocalDateTime.now())
        .build();
  }
}
