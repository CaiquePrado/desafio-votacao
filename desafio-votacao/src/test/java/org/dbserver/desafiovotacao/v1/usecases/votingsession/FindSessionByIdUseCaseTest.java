package org.dbserver.desafiovotacao.v1.usecases.votingsession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VotingSessionRepository;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.impl.FindSessionByIdUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindSessionByIdUseCaseTest {

  @Mock
  VotingSessionRepository votingSessionRepository;

  @InjectMocks
  FindSessionByIdUseCaseImpl findSessionByIdUseCaseImpl;

  UUID id = UUID.fromString("c0627815-2e84-4576-9791-61401449670a");
  Topic topic = TopicEntitiesBuiler.validTopic().toBuilder().id(id).build();
  VotingSession session = VotingSessionEntitiesBuilder.validVotingSessionWithId(id, topic);

  @DisplayName("Given a session id When finding by id Then return the session object")
  @Test
  void shouldFindSessionByIdAndReturnSessionObject() {

    given(votingSessionRepository.findById(any(UUID.class))).willReturn(Optional.of(session));

    VotingSession foundSession = findSessionByIdUseCaseImpl.find(id);

    assertNotNull(foundSession);
    assertEquals(id, foundSession.getId());
    assertEquals(session.getOpeningTime(), foundSession.getOpeningTime());
    assertEquals(session.getClosingTime(), foundSession.getClosingTime());
    assertEquals(session.getTopic(), foundSession.getTopic());
    verify(votingSessionRepository, times(1)).findById(id);
  }

  @DisplayName("Given a session id When finding by id and no session is found Then throw ResourceNotFoundException")
  @Test
  void shouldThrowResourceNotFoundExceptionWhenNoSessionFound() {

    UUID id = UUID.randomUUID();

    given(votingSessionRepository.findById(id)).willReturn(Optional.empty());

    var result = assertThrows(ResourceNotFoundException.class, () -> {
      findSessionByIdUseCaseImpl.find(id);
    });

    assertEquals("No session found.", result.getMessage());
    verify(votingSessionRepository, times(1)).findById(id);
  }
}
