package org.dbserver.desafiovotacao.v1.utils;

public class SqlProvider {

  public static final String clearDB = "/clear.sql";
  public static final String insertUsers = "/insertUsers.sql";
  public static final String insertTopics = "/insertTopics.sql";
  public static final String insertSessions = "/insertSessions.sql";

}
