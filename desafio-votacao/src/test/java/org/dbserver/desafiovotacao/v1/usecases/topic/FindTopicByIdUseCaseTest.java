package org.dbserver.desafiovotacao.v1.usecases.topic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.impl.FindTopicByIdUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindTopicByIdUseCaseTest {

  @Mock
  TopicRepository topicRepository;

  @InjectMocks
  FindTopicByIdUseCaseImpl findTopicByIdUseCaseImpl;

  UUID id = UUID.randomUUID();
  Topic topic = TopicEntitiesBuiler.validTopic().toBuilder().id(id).build();

  @DisplayName("Given a topic id When finding by id Then return the topic object")
  @Test
  void shouldFindTopicByIdAndReturnTopicObject() {

    given(topicRepository.findById(any(UUID.class))).willReturn(Optional.of(topic));

    Topic foundTopic = findTopicByIdUseCaseImpl.find(id);

    assertNotNull(foundTopic);
    assertEquals(id, foundTopic.getId());
    assertEquals(topic.getTitle(), foundTopic.getTitle());
    assertEquals(topic.getDescription(), foundTopic.getDescription());
    assertEquals(topic.getTopicStatus(), foundTopic.getTopicStatus());
    verify(topicRepository, times(1)).findById(id);
  }

  @DisplayName("Given a topic id When finding by id and no topic is found Then throw ResourceNotFoundException")
  @Test
  void shouldThrowResourceNotFoundExceptionWhenNoTopicFound() {

    UUID id = UUID.randomUUID();

    given(topicRepository.findById(id)).willReturn(Optional.empty());

    var result = assertThrows(ResourceNotFoundException.class, () -> {
      findTopicByIdUseCaseImpl.find(id);
    });

    assertEquals("No topic found.", result.getMessage());
    verify(topicRepository, times(1)).findById(id);
  }
}
