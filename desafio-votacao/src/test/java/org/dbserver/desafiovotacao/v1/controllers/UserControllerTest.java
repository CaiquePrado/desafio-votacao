package org.dbserver.desafiovotacao.v1.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.requests.CreateorUpdateuserRequestDTO;
import org.dbserver.desafiovotacao.v1.utils.SqlProvider;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;           

    @Autowired
    UserRepository userRepository;

    String sendAsJson;

    @DisplayName("Given Valid User Data when Create User should Return User and StatusCode 201")
    @Test
    void shouldReturnUserAndStatusCode201WhenCreateUserWithValidData() throws Exception, JsonProcessingException {

        CreateorUpdateuserRequestDTO createTopicRequestDTO = UserEntitiesBuilder.validCreateUserStub();
        sendAsJson = mapper.writeValueAsString(createTopicRequestDTO);

        mockMvc
                .perform(MockMvcRequestBuilders.post("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sendAsJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").value(createTopicRequestDTO.firstName()))
                .andExpect(jsonPath("$.lastName").value(createTopicRequestDTO.lastName()))
                .andExpect(jsonPath("$.age").value(createTopicRequestDTO.age()))
                .andExpect(jsonPath("$.cpf").value(createTopicRequestDTO.cpf()));
    }

    @DisplayName("Given Invalid User Data when Create User should Return StatusCode 400")
    @Test
    void shouldReturnStatusCode400WhenCreateUserWithInvalidData() throws Exception, JsonProcessingException {

        CreateorUpdateuserRequestDTO createTopicRequestDTO = UserEntitiesBuilder.invalidCreateUserStub();
        sendAsJson = mapper.writeValueAsString(createTopicRequestDTO);

        mockMvc
                .perform(MockMvcRequestBuilders.post("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sendAsJson))
                .andExpect(status().isBadRequest());
    }

    @DisplayName("Given Valid CPF when Find One User then Return User and StatusCode 200")
    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertUsers),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
    })
    void shouldReturnUserAndStatusCode200WhenFindOneUserWithValidCPF() throws Exception, JsonProcessingException {
        var cpf = userRepository.findAll().get(0).getCpf();

        mockMvc
                .perform(MockMvcRequestBuilders.get("/user/" + cpf))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cpf").value(cpf));
    }

    @DisplayName("Given Invalid CPF when Find One User then Return NotFound and StatusCode 404")
    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertUsers),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
    })
    void shouldReturnStatusCode404WhenFindOneUserWithInvalidCPF() throws Exception, JsonProcessingException {
        var cpf = "invalid_cpf";

        mockMvc
                .perform(MockMvcRequestBuilders.get("/user/" + cpf))
                .andExpect(status().isNotFound());
    }

    @DisplayName("Given Page and Size when List All Users then Return Users and StatusCode 200")
    @Test
    void shouldReturnUsersAndStatusCode200WhenListAllUsers() throws Exception, JsonProcessingException {
        int page = 0;
        int size = 10;

        mockMvc
                .perform(MockMvcRequestBuilders.get("/user")
                        .param("page", String.valueOf(page))
                        .param("size", String.valueOf(size)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

   @DisplayName("Given Valid CPF when Delete One User then Return StatusCode 204")
   @Test
   @SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertUsers),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
    })
    void shouldReturnStatusCode204WhenDeleteOneUserWithValidCPF() throws Exception, JsonProcessingException {

      var cpf = userRepository.findAll().get(1).getCpf();

    mockMvc
            .perform(MockMvcRequestBuilders.delete("/user/" + cpf))
            .andExpect(status().isNoContent());
   }

   @DisplayName("Given Invalid CPF when Find One User then Return NotFound and StatusCode 404")
   @Test
    void shouldReturnStatusCode404WhenDeleteOneUserWithInvalidCPF() throws Exception, JsonProcessingException {
        var cpf = "invalid_cpf";

        mockMvc
                .perform(MockMvcRequestBuilders.delete("/user/" + cpf))
                .andExpect(status().isNotFound());
    }

    @DisplayName("Given Valid User Data when Update User then Return User and StatusCode 200")
    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertUsers),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
    })
    void shouldReturnUserAndStatusCode200WhenUpdateUserWithValidData() throws Exception, JsonProcessingException {

        var cpf = userRepository.findAll().get(2).getCpf();

        CreateorUpdateuserRequestDTO updateUserRequestDTO = UserEntitiesBuilder.validUpdateUserStub();
        sendAsJson = mapper.writeValueAsString(updateUserRequestDTO);

        mockMvc
                .perform(MockMvcRequestBuilders.put("/user/" + cpf)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sendAsJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(updateUserRequestDTO.firstName()))
                .andExpect(jsonPath("$.lastName").value(updateUserRequestDTO.lastName()))
                .andExpect(jsonPath("$.age").value(updateUserRequestDTO.age()));
    }

    @SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertUsers),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
    })
    @DisplayName("Given Valid User Data when Update User then Return User and StatusCode 400")
    @Test
    void shouldReturnStatusCode400WhenUpdateUserWitInhalidData() throws Exception, JsonProcessingException {

        var cpf = "invalid_cpf";

        CreateorUpdateuserRequestDTO updateUserRequestDTO = UserEntitiesBuilder.validUpdateUserStub();
        sendAsJson = mapper.writeValueAsString(updateUserRequestDTO);

        mockMvc
                .perform(MockMvcRequestBuilders.put("/user/" + cpf)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(sendAsJson))
                .andExpect(status().isNotFound());
    }
}
