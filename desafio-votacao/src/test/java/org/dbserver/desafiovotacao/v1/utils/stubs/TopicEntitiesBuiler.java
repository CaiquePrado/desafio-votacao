package org.dbserver.desafiovotacao.v1.utils.stubs;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.requests.CreateTopicRequestDTO;

public class TopicEntitiesBuiler {

  public static CreateTopicRequestDTO validCreateTopicStub() {
    return CreateTopicRequestDTO.builder()
        .title("Lorem ipsum")
        .description("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
        .build();
  }

  public static CreateTopicRequestDTO invalidCreateTopicStub() {
    return CreateTopicRequestDTO.builder()
        .title("")
        .description("")
        .build();
  }

  public static Topic validTopic() {
    return Topic.builder()
        .title("Lorem ipsum")
        .description("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
        .topicStatus(TopicStatus.STARTED)
        .build();
  }

  public static Topic validTopicStatus(TopicStatus status) {
    return Topic.builder()
        .title("Lorem ipsum")
        .description("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
        .topicStatus(status)
        .build();
  }
}
