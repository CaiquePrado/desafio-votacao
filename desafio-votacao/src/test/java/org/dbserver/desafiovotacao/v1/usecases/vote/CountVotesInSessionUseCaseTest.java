package org.dbserver.desafiovotacao.v1.usecases.vote;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VoteRepository;
import org.dbserver.desafiovotacao.v1.usecases.vote.impl.CountVotesInSessionUseCaseImpl;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.dbserver.desafiovotacao.v1.utils.stubs.VoteEntitiesBuilder;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CountVotesInSessionUseCaseTest {
  @Mock
  VoteRepository voteRepository;

  @Mock
  FindSessionByIdUseCase findSessionByIdUseCase;

  @InjectMocks
  CountVotesInSessionUseCaseImpl countVotesInSessionUseCaseImpl;

  UUID sessionId = UUID.fromString("c0627815-2e84-4576-9791-61401449670a");
  User user = UserEntitiesBuilder.validUser();
  Topic topic = TopicEntitiesBuiler.validTopic().toBuilder().id(sessionId).build();
  VotingSession session = VotingSessionEntitiesBuilder.stubVotingSession(sessionId, topic);
  VoteDecision decisionYes = VoteDecision.YES;
  VoteDecision decisionNo = VoteDecision.NO;
  Vote voteYes = VoteEntitiesBuilder.validVote(user, session, decisionYes);
  Vote voteNo = VoteEntitiesBuilder.validVote(user, session, decisionNo);

  @DisplayName("Given a session id When executing Then return the count of votes")
  @Test
  void shouldExecuteAndReturnCountOfVotes() {

    given(findSessionByIdUseCase.find(sessionId)).willReturn(session);
    given(voteRepository.findByVotingSession(session)).willReturn(Arrays.asList(voteYes, voteNo, voteNo));
    Map<VoteDecision, Long> voteCount = countVotesInSessionUseCaseImpl.execute(sessionId);

    assertEquals(1L, voteCount.get(decisionYes));
    assertEquals(2L, voteCount.get(decisionNo));

    verify(findSessionByIdUseCase).find(sessionId);
    verify(voteRepository).findByVotingSession(session);
  }
}
