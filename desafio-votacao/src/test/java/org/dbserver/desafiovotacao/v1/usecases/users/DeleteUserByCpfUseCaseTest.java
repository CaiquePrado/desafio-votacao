package org.dbserver.desafiovotacao.v1.usecases.users;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.impl.DeleteUserByCpfUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DeleteUserByCpfUseCaseTest {

  @Mock
  UserRepository userRepository;

  @Mock
  FindUserByCpfUseCase findUserByCpfUseCase;

  @InjectMocks
  DeleteUserByCpfUseCaseImpl deleteUserByCpfUseCaseImpl;

  User user = UserEntitiesBuilder.validUser();

  @DisplayName("Given User CPF when Delete User should Execute Without Exception")
  @Test
  void shouldDeleteUserByCpfAndExecuteWithoutException() {

    given(findUserByCpfUseCase.find(anyString())).willReturn(user);

    assertDoesNotThrow(() -> {
      deleteUserByCpfUseCaseImpl.execute(user.getCpf());
    });

    verify(userRepository, times(1)).delete(user);
  }

  @DisplayName("Given Non-Existing User CPF when Delete User should Throw ResourceNotFoundException")
  @Test
  void shouldThrowResourceNotFoundExceptionWhenDeleteNonExistingUserByCpf() {

    given(findUserByCpfUseCase.find(anyString())).willThrow(new ResourceNotFoundException("No user found."));

    ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> {
      deleteUserByCpfUseCaseImpl.execute(user.getCpf());
    });

    assertEquals("No user found.", result.getMessage());
    verify(userRepository, never()).delete(any(User.class));
  }
}
