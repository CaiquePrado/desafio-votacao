package org.dbserver.desafiovotacao.v1.utils.stubs;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.requests.CreateVoteRequestDTO;

public class VoteEntitiesBuilder {

  public static Vote validVote(User user, VotingSession votingSession, VoteDecision decision) {
    return Vote.builder()
        .id(UUID.randomUUID())
        .user(user)
        .votingSession(votingSession)
        .decision(decision)
        .createdAt(LocalDateTime.now())
        .build();
  }

  public static CreateVoteRequestDTO createVoteRequesStub() {
    return CreateVoteRequestDTO.builder()
        .cpf("97362859015")
        .decision(VoteDecision.YES)
        .build();
  }
}
