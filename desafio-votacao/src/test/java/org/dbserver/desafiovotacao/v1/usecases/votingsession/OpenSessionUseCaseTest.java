package org.dbserver.desafiovotacao.v1.usecases.votingsession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.UUID;
import java.util.stream.Stream;

import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VotingSessionRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.FindTopicByIdUseCase;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.impl.OpenSessionUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OpenSessionUseCaseTest {

  @Mock
  VotingSessionRepository votingSessionRepository;

  @Mock
  FindTopicByIdUseCase findTopicByIdUseCase;

  @InjectMocks
  OpenSessionUseCaseImpl openSessionUseCaseImpl;

  @ParameterizedTest
  @MethodSource("provideSessionsAndTopics")
  @DisplayName("Given a voting session and topic id When executing open session Then return the saved session")
  void shouldOpenSessionAndReturnSavedSession(VotingSession votingSession, UUID topicId, Integer duration,
      VotingSession expectedSession) {

    Topic topic = TopicEntitiesBuiler.validTopic();
    topic.setId(topicId);

    given(findTopicByIdUseCase.find(topicId)).willReturn(topic);
    given(votingSessionRepository.save(votingSession)).willReturn(expectedSession);

    VotingSession savedSession = openSessionUseCaseImpl.execute(votingSession, topicId, duration);

    assertNotNull(savedSession);
    assertEquals(expectedSession, savedSession);
    verify(findTopicByIdUseCase, times(1)).find(topicId);
    verify(votingSessionRepository, times(1)).save(votingSession);
  }

  public static Stream<Arguments> provideSessionsAndTopics() {
    return Stream.of(
        Arguments.of(VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic()),
            UUID.randomUUID(), 1,
            VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic())),
        Arguments.of(VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic()),
            UUID.randomUUID(), null,
            VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic())),
        Arguments.of(VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic()),
            UUID.randomUUID(), 5,
            VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic())));
  }
}
