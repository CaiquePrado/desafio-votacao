package org.dbserver.desafiovotacao.v1.usecases.users;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.dbserver.desafiovotacao.v1.exceptions.InvalidRequestException;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.impl.UpdateUserUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UpdateUserUseCaseTest {

  @Mock
  UserRepository userRepository;

  @Mock
  FindUserByCpfUseCase findUserByCpfUseCase;

  @InjectMocks
  UpdateUserUseCaseImpl updateUserUseCaseImpl;

  User existingUser = UserEntitiesBuilder.validUser();
  User newUser = UserEntitiesBuilder.updatedUser();

  @DisplayName("Given User and CPF when Update User should Return Updated User")
  @Test
  void shouldUpdateUserAndReturnUpdatedUser() {

    given(findUserByCpfUseCase.find(anyString())).willReturn(existingUser);
    given(userRepository.save(any(User.class))).willAnswer(invocation -> invocation.getArgument(0));

    User updatedUser = updateUserUseCaseImpl.execute(newUser, existingUser.getCpf());

    assertNotNull(updatedUser);
    assertEquals(newUser.getFirstName(), updatedUser.getFirstName());
    assertEquals(newUser.getLastName(), updatedUser.getLastName());
    assertEquals(newUser.getAge(), updatedUser.getAge());
    assertEquals(newUser.getCpf(), updatedUser.getCpf());
  }

  @DisplayName("Given Invalid CPF when Update User should Throw InvalidRequestException")
  @Test
  void shouldThrowInvalidRequestExceptionWhenUpdateUserWithInvalidCPF() {

    given(findUserByCpfUseCase.find(anyString()))
        .willThrow(new InvalidRequestException("A user with these CPF already exists."));

    Exception exception = assertThrows(InvalidRequestException.class, () -> {
      updateUserUseCaseImpl.execute(newUser, "invalid_cpf");
    });

    assertEquals("A user with these CPF already exists.", exception.getMessage());
    verify(userRepository, never()).save(any(User.class));
  }
}
