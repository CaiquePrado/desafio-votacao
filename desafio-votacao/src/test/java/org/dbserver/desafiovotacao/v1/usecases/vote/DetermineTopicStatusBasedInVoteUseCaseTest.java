package org.dbserver.desafiovotacao.v1.usecases.vote;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;
import java.util.stream.Stream;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.repository.VoteRepository;
import org.dbserver.desafiovotacao.v1.usecases.vote.impl.DetermineTopicStatusBasedInVoteUseCaseImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DetermineTopicStatusBasedInVoteUseCaseTest {

  @Mock
  VoteRepository voteRepository;

  @InjectMocks
  DetermineTopicStatusBasedInVoteUseCaseImpl determineTopicStatusBasedInVoteUseCaseImpl;

  @ParameterizedTest
  @MethodSource("provideVoteCounts")
  @DisplayName("Given a map of vote counts When executing Then return the correct topic status")
  void shouldExecuteAndReturnCorrectTopicStatus(Map<VoteDecision, Long> voteCounts, TopicStatus expectedStatus) {
    TopicStatus status = determineTopicStatusBasedInVoteUseCaseImpl.execute(voteCounts);

    assertEquals(expectedStatus, status);
  }

  public static Stream<Arguments> provideVoteCounts() {
    return Stream.of(
        Arguments.of(Map.of(VoteDecision.YES, 3L, VoteDecision.NO, 2L), TopicStatus.APPROVED),
        Arguments.of(Map.of(VoteDecision.YES, 2L, VoteDecision.NO, 3L), TopicStatus.REJECTED),
        Arguments.of(Map.of(VoteDecision.YES, 2L, VoteDecision.NO, 2L), TopicStatus.TIED));
  }
}
