package org.dbserver.desafiovotacao.v1.usecases.topic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.exceptions.InvalidStatusException;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.impl.FindTopicsByStatusUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindTopicsByStatusUseCaseTest {

  @Mock
  TopicRepository topicRepository;

  @InjectMocks
  FindTopicsByStatusUseCaseImpl findTopicsByStatusUseCaseImpl;

  List<Topic> topics;

  @ParameterizedTest
  @MethodSource("provideTopicSatus")
  @DisplayName("Given a topic status When finding topics by status Then return the topics")
  void shouldFindTopicsByStatusAndReturnTopics(String status, List<Topic> expectedTopics) {

    given(topicRepository.findByTopicStatusIgnoreCase(status)).willReturn(expectedTopics);

    List<Topic> foundTopics = findTopicsByStatusUseCaseImpl.execute(status);

    assertNotNull(foundTopics);
    assertEquals(expectedTopics.size(), foundTopics.size());
    verify(topicRepository, times(1)).findByTopicStatusIgnoreCase(status);
  }

  public static Stream<Arguments> provideTopicSatus() {
    return Stream.of(
        Arguments.of(TopicStatus.REJECTED.name(),
            Arrays.asList(TopicEntitiesBuiler.validTopicStatus(TopicStatus.REJECTED),
                TopicEntitiesBuiler.validTopicStatus(TopicStatus.REJECTED))),
        Arguments.of(TopicStatus.APPROVED.name(),
            Arrays.asList(TopicEntitiesBuiler.validTopicStatus(TopicStatus.APPROVED))),
        Arguments.of(TopicStatus.STARTED.name(), new ArrayList<>()));
  }

  @DisplayName("Given an invalid status When finding topics by status Then throw InvalidStatusException")
  @Test
  void shouldThrowInvalidStatusExceptionWhenStatusIsInvalid() {

    String invalidStatus = "INVALID_STATUS";

    var result = assertThrows(InvalidStatusException.class, () -> {
      findTopicsByStatusUseCaseImpl.execute(invalidStatus);
    });

    assertEquals("Invalid status: " + invalidStatus, result.getMessage());
  }
}
