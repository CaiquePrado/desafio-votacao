package org.dbserver.desafiovotacao.v1.usecases.users;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.dbserver.desafiovotacao.v1.exceptions.InvalidRequestException;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.user.impl.CreateUserUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CreateUserUseCaseTest {

  @Mock
  UserRepository userRepository;

  @Mock
  FindUserByCpfUseCase findUserByCpfUseCase;

  @InjectMocks
  CreateUserUseCaseImpl createUserUseCaseImpl;

  User user = UserEntitiesBuilder.validUser();

  @DisplayName("Given User Object When Save User Ten Return User")
  @Test
  void shouldSaveUserAndReturnSavedObject() {

    given(userRepository.save(any(User.class))).willAnswer(invocation -> invocation.getArgument(0));

    User savedUser = createUserUseCaseImpl.execute(user);

    assertNotNull(savedUser);
    assertEquals(user.getFirstName(), savedUser.getFirstName());
    assertEquals(user.getLastName(), savedUser.getLastName());
    assertEquals(user.getAge(), savedUser.getAge());
    assertEquals(user.getCpf(), savedUser.getCpf());
  }

  @DisplayName("Given User Object with Existing CPF When Save User Then Throw InvalidRequestException")
  @Test
  void shouldThrowInvalidRequestExceptionWhenSaveUserWithExistingCPF() {

    doThrow(new InvalidRequestException("A user with these CPF already exists.")).when(findUserByCpfUseCase)
        .checkIfCpfExists(anyString());

    assertThrows(InvalidRequestException.class, () -> {
      createUserUseCaseImpl.execute(user);
    });

    verify(userRepository, never()).save(any(User.class));
  }
}
