package org.dbserver.desafiovotacao.v1.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.requests.CreateTopicRequestDTO;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

class TopicMapperTest {

  TopicMapper topicMapper = Mappers.getMapper(TopicMapper.class);

  CreateTopicRequestDTO createTopicRequestDTO = TopicEntitiesBuiler.validCreateTopicStub();

  @DisplayName("Given CreateTopicRequestDTO Object When Map Then Return Topic")
  @Test
  void shouldMapCreateTopicRequestDTOToTopic() {

    Topic topic = topicMapper.toTopic(createTopicRequestDTO);

    assertNotNull(topic);
    assertNull(topic.getId());
    assertNull(topic.getCreatedAt());
    assertNull(topic.getVotingSession());
    assertEquals(createTopicRequestDTO.title(), topic.getTitle());
    assertEquals(createTopicRequestDTO.description(), topic.getDescription());
    assertEquals(TopicStatus.STARTED, topic.getTopicStatus());
  }
}