package org.dbserver.desafiovotacao.v1.usecases.topic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.UUID;
import java.util.stream.Stream;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.impl.UpdateTopicStatusUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UpdateTopicStatusUseCaseTest {

  @Mock
  TopicRepository topicRepository;

  @Mock
  FindTopicByIdUseCase findTopicByIdUseCase;

  @InjectMocks
  UpdateTopicStatusUseCaseImpl updateTopicStatusUseCaseImpl;

  @ParameterizedTest
  @MethodSource("provideTopicsAndStatus")
  @DisplayName("Given a topic id and status When updating topic status Then return the updated topic")
  void shouldUpdateTopicStatusAndReturnUpdatedTopic(UUID topicId, TopicStatus topicStatus, Topic expectedTopic) {

    given(findTopicByIdUseCase.find(topicId)).willReturn(expectedTopic);
    given(topicRepository.save(expectedTopic)).willReturn(expectedTopic);

    Topic updatedTopic = updateTopicStatusUseCaseImpl.execute(topicId, topicStatus);

    assertNotNull(updatedTopic);
    assertEquals(expectedTopic, updatedTopic);
    verify(findTopicByIdUseCase, times(1)).find(topicId);
    verify(topicRepository, times(1)).save(expectedTopic);
  }

  public static Stream<Arguments> provideTopicsAndStatus() {
    return Stream.of(
        Arguments.of(UUID.randomUUID(), TopicStatus.REJECTED,
            TopicEntitiesBuiler.validTopicStatus(TopicStatus.REJECTED)),
        Arguments.of(UUID.randomUUID(), TopicStatus.APPROVED,
            TopicEntitiesBuiler.validTopicStatus(TopicStatus.APPROVED)),
        Arguments.of(UUID.randomUUID(), TopicStatus.STARTED, TopicEntitiesBuiler.validTopicStatus(TopicStatus.STARTED)),
        Arguments.of(UUID.randomUUID(), TopicStatus.CLOSED, TopicEntitiesBuiler.validTopicStatus(TopicStatus.CLOSED)));
  }
}
