package org.dbserver.desafiovotacao.v1.usecases.vote;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.models.Vote;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VoteRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.impl.VoteInSessionUseCaseImpl;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.dbserver.desafiovotacao.v1.utils.stubs.VoteEntitiesBuilder;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class VoteInSessionUseCaseTest {
  @Mock
  VoteValidationUseCase voteValidationUseCase;

  @Mock
  FindUserByCpfUseCase findUserByCpfUseCase;

  @Mock
  FindSessionByIdUseCase findSessionByIdUseCase;

  @Mock
  VoteRepository voteRepository;

  @InjectMocks
  VoteInSessionUseCaseImpl voteInSessionUseCaseImpl;

  String cpf = "74300697035";
  UUID sessionId = UUID.fromString("c0627815-2e84-4576-9791-61401449670a");
  User user = UserEntitiesBuilder.validUser();
  Topic topic = TopicEntitiesBuiler.validTopic().toBuilder().id(sessionId).build();
  VotingSession session = VotingSessionEntitiesBuilder.stubVotingSession(sessionId, topic);
  VoteDecision decision = VoteDecision.YES;
  Vote vote = VoteEntitiesBuilder.validVote(user, session, decision);

  @BeforeEach
  void setUp() {
    given(findUserByCpfUseCase.find(cpf)).willReturn(user);
    given(findSessionByIdUseCase.find(sessionId)).willReturn(session);
  }

  @DisplayName("Given a vote, cpf, session id and decision When executing Then return the saved vote")
  @Test
  void shouldExecuteAndReturnSavedVote() {
    given(voteRepository.save(any(Vote.class))).willAnswer(invocation -> invocation.getArgument(0));

    Vote savedVote = voteInSessionUseCaseImpl.execute(vote, cpf, sessionId, decision);

    assertEquals(user, savedVote.getUser());
    assertEquals(session, savedVote.getVotingSession());
    assertEquals(decision, savedVote.getDecision());

    verify(voteValidationUseCase).validate(cpf, sessionId);
    verify(voteRepository).save(vote);
  }

  @DisplayName("Given a vote, cpf, session id and decision NO When executing Then return the saved vote")
  @Test
  void shouldExecuteAndReturnSavedVoteWithDecisionNo() {
    VoteDecision decisionNo = VoteDecision.NO;
    Vote voteNo = VoteEntitiesBuilder.validVote(user, session, decisionNo);

    given(voteRepository.save(any(Vote.class))).willReturn(voteNo);

    Vote savedVote = voteInSessionUseCaseImpl.execute(voteNo, cpf, sessionId, decisionNo);

    assertEquals(user, savedVote.getUser());
    assertEquals(session, savedVote.getVotingSession());
    assertEquals(decisionNo, savedVote.getDecision());

    verify(voteValidationUseCase).validate(cpf, sessionId);
    verify(voteRepository).save(voteNo);
  }
}
