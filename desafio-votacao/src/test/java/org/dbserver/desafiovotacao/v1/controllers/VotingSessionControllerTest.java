package org.dbserver.desafiovotacao.v1.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.requests.CreateVotingSessionRequestDTO;
import org.dbserver.desafiovotacao.v1.utils.SqlProvider;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
class VotingSessionControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper mapper;

  @Autowired
  TopicRepository topicRepository;

  String sendAsJson;

  @DisplayName("Given Valid Voting Session Data when Open Voting Session then Return Voting Session and StatusCode 201")
  @Test
  @SqlGroup({
      @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTopics),
      @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  })
  void shouldReturnVotingSessionAndStatusCode201WhenOpenVotingSessionWithValidData()
      throws Exception, JsonProcessingException {

    var topic = topicRepository.findAll().get(0).getId();

    CreateVotingSessionRequestDTO createVotingSessionRequestDTO = VotingSessionEntitiesBuilder
        .validCreateVotingSessioncStub();
    sendAsJson = mapper.writeValueAsString(createVotingSessionRequestDTO);

    mockMvc.perform(MockMvcRequestBuilders.post("/votingsession/" + topic)
        .contentType(MediaType.APPLICATION_JSON)
        .content(sendAsJson))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.duration").value(createVotingSessionRequestDTO.duration()));
  }

  @DisplayName("Given Invalid Voting Session Data when Open Voting Session then Return BadRequest and StatusCode 400")
  @Test
  void shouldReturnStatusCode400WhenOpenVotingSessionWithInvalidTopicId()
      throws Exception, JsonProcessingException {

    var topic = UUID.randomUUID();

    CreateVotingSessionRequestDTO createVotingSessionRequestDTO = VotingSessionEntitiesBuilder
        .validCreateVotingSessioncStub();
    sendAsJson = mapper.writeValueAsString(createVotingSessionRequestDTO);

    mockMvc.perform(MockMvcRequestBuilders.post("/votingsession/" + topic)
        .contentType(MediaType.APPLICATION_JSON)
        .content(sendAsJson))
        .andExpect(status().isNotFound());
  }
}