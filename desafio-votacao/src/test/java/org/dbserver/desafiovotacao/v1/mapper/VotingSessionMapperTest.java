package org.dbserver.desafiovotacao.v1.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.requests.CreateVotingSessionRequestDTO;
import org.dbserver.desafiovotacao.v1.responses.VoteResutResponseDTO;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

class VotingSessionMapperTest {

  VotingSessionMapper votingSessionMapper = Mappers.getMapper(VotingSessionMapper.class);

  CreateVotingSessionRequestDTO createVotingSessionRequestDTO = VotingSessionEntitiesBuilder
      .validCreateVotingSessioncStub();

  @DisplayName("Given CreateVotingSessionRequestDTO Object When Map Then Return VotingSession")
  @Test
  void shouldMapCreateVotingSessionRequestDTOToVotingSession() {

    VotingSession votingSession = votingSessionMapper.toVotingSession(createVotingSessionRequestDTO);

    assertNotNull(votingSession);
    assertNull(votingSession.getId());
    assertNull(votingSession.getClosingTime());
    assertNull(votingSession.getCreatedAt());
    assertEquals(createVotingSessionRequestDTO.duration(), votingSession.getDuration());
  }

  @DisplayName("Given VoteCounts and TopicStatus When Map Then Return VoteResutResponseDTO")
  @Test
  void shouldMapVoteCountsAndTopicStatusToVoteResult() {

    Map<VoteDecision, Long> voteCounts = new HashMap<>();
    voteCounts.put(VoteDecision.YES, 10L);
    voteCounts.put(VoteDecision.NO, 5L);
    TopicStatus topicStatus = TopicStatus.APPROVED;

    VoteResutResponseDTO voteResult = votingSessionMapper.toVoteResult(voteCounts, topicStatus);

    assertNotNull(voteResult);
    assertEquals(voteCounts, voteResult.voteCounts());
    assertEquals(topicStatus, voteResult.topicStatus());
  }
}
