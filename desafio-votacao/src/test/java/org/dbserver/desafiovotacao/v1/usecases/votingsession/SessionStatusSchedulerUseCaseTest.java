package org.dbserver.desafiovotacao.v1.usecases.votingsession;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.usecases.vote.UpdateTopicStatusBasedOnVoteUse;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.impl.SessionStatusSchedulerUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SessionStatusSchedulerUseCaseTest {

  @Mock
  private UpdateTopicStatusBasedOnVoteUse updateTopicStatusBasedOnVoteUse;

  @Mock
  private FindSessionByIdUseCase findSessionByIdUseCase;

  @InjectMocks
  private SessionStatusSchedulerUseCaseImpl sessionStatusSchedulerUseCaseImpl;

  @Test
  @DisplayName("Given a session When updating session status Then verify the status update")
  void shouldUpdateSessionStatus() {
    UUID sessionId = UUID.randomUUID();
    VotingSession session = VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic());
    session.setClosingTime(LocalDateTime.now().minusMinutes(1));

    given(findSessionByIdUseCase.find(sessionId)).willReturn(session);

    sessionStatusSchedulerUseCaseImpl.updateSessionStatus(sessionId);

    verify(findSessionByIdUseCase, times(1)).find(sessionId);
  }

  @Test
  @DisplayName("Given a session that has not ended When updating session status Then do nothing")
  void shouldDoNothingWhenSessionHasNotEnded() {
    UUID sessionId = UUID.randomUUID();
    VotingSession session = VotingSessionEntitiesBuilder.validVotingSession(TopicEntitiesBuiler.validTopic());
    session.setClosingTime(LocalDateTime.now().plusMinutes(1));

    given(findSessionByIdUseCase.find(sessionId)).willReturn(session);

    sessionStatusSchedulerUseCaseImpl.updateSessionStatus(sessionId);

    verify(findSessionByIdUseCase, times(1)).find(sessionId);
    verifyNoInteractions(updateTopicStatusBasedOnVoteUse);
    assertEquals(TopicStatus.STARTED, session.getTopic().getTopicStatus());

  }
}
