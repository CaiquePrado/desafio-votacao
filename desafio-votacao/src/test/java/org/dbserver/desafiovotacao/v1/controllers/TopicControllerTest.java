package org.dbserver.desafiovotacao.v1.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.requests.CreateTopicRequestDTO;
import org.dbserver.desafiovotacao.v1.utils.SqlProvider;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
class TopicControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper mapper;

  @Autowired
  TopicRepository topicRepository;

  String sendAsJson;

  @DisplayName("Given TopicDto When Save Should Create Topic and ReturnStatus201")
  @Test
  void shouldBeAbleToSaveNewTopicAndReturnStatusCode201() throws Exception, JsonProcessingException {

    CreateTopicRequestDTO createTopicRequestDTO = TopicEntitiesBuiler.validCreateTopicStub();
    sendAsJson = mapper.writeValueAsString(createTopicRequestDTO);

    mockMvc
        .perform(MockMvcRequestBuilders.post("/topic")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendAsJson))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.title").value(createTopicRequestDTO.title()))
        .andExpect(jsonPath("$.description").value(createTopicRequestDTO.description()));
  }

  @DisplayName("Given an Invalid TopicDto When Save Should ReturnS tatus400")
  @Test
  void ShouldReturnStatus400WhenTryingToRegisterAnInvalidTopic() throws Exception, JsonProcessingException {
    CreateTopicRequestDTO createTopicRequestDTO = TopicEntitiesBuiler.invalidCreateTopicStub();
    String sendAsJson = mapper.writeValueAsString(createTopicRequestDTO);

    mockMvc
        .perform(MockMvcRequestBuilders.post("/topic")
            .contentType(MediaType.APPLICATION_JSON)
            .content(sendAsJson))
        .andExpect(status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = { "STARTED", "APPROVED", "CLOSED", "REJECTED", "TIED" })
  @SqlGroup({
      @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTopics),
      @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  })
  void shouldReturnTopicsAndStatusCode200WhenFindTopicsByValidStatus(String status)
      throws Exception, JsonProcessingException {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/topic/" + status))
        .andExpect(status().isOk());
  }
}
