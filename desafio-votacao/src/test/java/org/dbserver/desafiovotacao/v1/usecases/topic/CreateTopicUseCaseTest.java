package org.dbserver.desafiovotacao.v1.usecases.topic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.repository.TopicRepository;
import org.dbserver.desafiovotacao.v1.usecases.topic.impl.CreateTopicUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CreateTopicUseCaseTest {

  @Mock
  TopicRepository topicRepository;

  @InjectMocks
  CreateTopicUseCaseImpl createTopicUseCaseImpl;

  @DisplayName("Given a topic When saving Then return the saved topic object")
  @Test
  void shouldSaveTopicAndReturnSavedObject() {

    Topic topic = TopicEntitiesBuiler.validTopic();

    given(topicRepository.save(any(Topic.class))).willAnswer(invocation -> invocation.getArgument(0));

    Topic savedTopic = createTopicUseCaseImpl.execute(topic);

    assertNotNull(savedTopic);
    assertEquals(topic.getTitle(), savedTopic.getTitle());
    assertEquals(topic.getDescription(), savedTopic.getDescription());
  }
}
