package org.dbserver.desafiovotacao.v1.usecases.vote;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.Map;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.enums.TopicStatus;
import org.dbserver.desafiovotacao.v1.enums.VoteDecision;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.usecases.topic.UpdateTopicStatusUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.impl.UpdateTopicStatusBasedOnVoteUseCaseImpl;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@ExtendWith(MockitoExtension.class)
class UpdateTopicStatusBasedOnVoteUseCaseTest {

  @Mock
  CountVotesInSessionUseCase countVotesInSessionUseCase;

  @Mock
  DetermineTopicStatusBasedInVoteUseCase determineTopicStatusUseCase;

  @Mock
  UpdateTopicStatusUseCase updateTopicStatusUseCase;

  @Mock
  FindSessionByIdUseCase findSessionByIdUseCase;

  @InjectMocks
  UpdateTopicStatusBasedOnVoteUseCaseImpl updateTopicStatusBasedOnVoteUseCaseImpl;

  UUID sessionId = UUID.fromString("c0627815-2e84-4576-9791-61401449670a");
  Map<VoteDecision, Long> voteCounts = Map.of(VoteDecision.YES, 3L, VoteDecision.NO, 2L);
  TopicStatus newStatus = TopicStatus.APPROVED;

  @DisplayName("Given a session id When executing Then update the topic status based on vote")
  @Test
  void shouldExecuteAndUpdateTopicStatusBasedOnVote() {
    Topic topic = TopicEntitiesBuiler.validTopic();
    VotingSession session = VotingSessionEntitiesBuilder.validVotingSession(topic);

    given(countVotesInSessionUseCase.execute(sessionId)).willReturn(voteCounts);
    given(determineTopicStatusUseCase.execute(voteCounts)).willReturn(newStatus);
    given(findSessionByIdUseCase.find(sessionId)).willReturn(session);

    updateTopicStatusBasedOnVoteUseCaseImpl.execute(sessionId);

    verify(countVotesInSessionUseCase).execute(sessionId);
    verify(determineTopicStatusUseCase).execute(voteCounts);
    verify(updateTopicStatusUseCase).execute(topic.getId(), newStatus);
  }
}
