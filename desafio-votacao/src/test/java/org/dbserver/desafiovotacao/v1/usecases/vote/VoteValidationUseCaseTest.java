package org.dbserver.desafiovotacao.v1.usecases.vote;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.UUID;

import org.dbserver.desafiovotacao.v1.exceptions.InvalidRequestException;
import org.dbserver.desafiovotacao.v1.models.Topic;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.models.VotingSession;
import org.dbserver.desafiovotacao.v1.repository.VoteRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.FindUserByCpfUseCase;
import org.dbserver.desafiovotacao.v1.usecases.vote.impl.VoteValidationUseCaseImpl;
import org.dbserver.desafiovotacao.v1.usecases.votingsession.FindSessionByIdUseCase;
import org.dbserver.desafiovotacao.v1.utils.stubs.TopicEntitiesBuiler;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.dbserver.desafiovotacao.v1.utils.stubs.VotingSessionEntitiesBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class VoteValidationUseCaseTest {

  @Mock
  FindUserByCpfUseCase findUserByCpfUseCase;

  @Mock
  FindSessionByIdUseCase findSessionByIdUseCase;

  @Mock
  VoteRepository voteRepository;

  @InjectMocks
  VoteValidationUseCaseImpl voteValidationUseCaseImpl;

  String cpf = "74300697035";
  UUID sessionId = UUID.fromString("c0627815-2e84-4576-9791-61401449670a");
  User user = UserEntitiesBuilder.validUser();
  Topic topic = TopicEntitiesBuiler.validTopic().toBuilder().id(sessionId).build();
  VotingSession session = VotingSessionEntitiesBuilder.stubVotingSession(sessionId, topic);

  @BeforeEach
  void setUp() {
    given(findUserByCpfUseCase.find(cpf)).willReturn(user);
    given(findSessionByIdUseCase.find(sessionId)).willReturn(session);
  }

  @DisplayName("Dado um cpf e id de sessão Quando validando Então nenhuma exceção é lançada")
  @Test
  void deveValidarENaoLancarExcecao() {

    given(voteRepository.hasUserVotedInSession(user, session)).willReturn(false);

    assertDoesNotThrow(() -> voteValidationUseCaseImpl.validate(cpf, sessionId));
  }

  @DisplayName("Dado um cpf e id de sessão Quando validando e o usuário já votou Então lançar InvalidRequestException")
  @Test
  void deveLancarInvalidRequestExceptionQuandoUsuarioJaVotou() {

    given(voteRepository.hasUserVotedInSession(user, session)).willReturn(true);

    assertThrows(InvalidRequestException.class, () -> voteValidationUseCaseImpl.validate(cpf, sessionId));
  }
}