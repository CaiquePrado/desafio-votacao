package org.dbserver.desafiovotacao.v1.usecases.users;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import java.util.UUID;

import org.dbserver.desafiovotacao.v1.exceptions.ResourceNotFoundException;
import org.dbserver.desafiovotacao.v1.models.User;
import org.dbserver.desafiovotacao.v1.repository.UserRepository;
import org.dbserver.desafiovotacao.v1.usecases.user.impl.FindUserByIdUseCaseImpl;
import org.dbserver.desafiovotacao.v1.utils.stubs.UserEntitiesBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FindUserByIdUseCaseTest {

  @Mock
  UserRepository userRepository;

  @InjectMocks
  FindUserByIdUseCaseImpl findUserByIdUseCaseImpl;

  @DisplayName("Given User ID when Find User should Return User")
  @Test
  void shouldFindUserByIdAndReturnUser() {
    UUID userId = UUID.randomUUID();
    User user = UserEntitiesBuilder.validUser().toBuilder().id(userId).build();

    given(userRepository.findById(any(UUID.class))).willReturn(Optional.of(user));

    User foundUser = findUserByIdUseCaseImpl.find(userId);

    assertNotNull(foundUser);
    assertEquals(user.getFirstName(), foundUser.getFirstName());
    assertEquals(user.getLastName(), foundUser.getLastName());
    assertEquals(user.getAge(), foundUser.getAge());
    assertEquals(user.getCpf(), foundUser.getCpf());
  }

  @DisplayName("Given Non-Existing User ID when Find User should Throw ResourceNotFoundException")
  @Test
  void shouldThrowResourceNotFoundExceptionWhenFindNonExistingUserById() {
    UUID userId = UUID.randomUUID();

    given(userRepository.findById(any(UUID.class))).willReturn(Optional.empty());

    ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> {
      findUserByIdUseCaseImpl.find(userId);
    });

    assertEquals("No user found.", result.getMessage());
  }
}
