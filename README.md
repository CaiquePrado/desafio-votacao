# Desafio votação
Este é um projeto Spring Boot que utiliza Java 17 e Gradle como sistema de build. Ele é destinado para criação de uma aplicação web utilizando Spring Framework.

## Pré-requisitos
Certifique-se de ter os seguintes pré-requisitos instalados antes de começar:

* Java 17: Certifique-se de ter o JDK 17 instalado em sua máquina. Você pode baixá-lo e instalá-lo a partir do site oficial da Oracle ou de outras fontes confiáveis.

* Gradle: Este projeto utiliza o Gradle como sistema de build. Certifique-se de ter o Gradle instalado em sua máquina. Você pode instalá-lo seguindo as instruções disponíveis no site oficial do Gradle.

* IDE de sua preferência: Recomenda-se o uso de uma IDE para desenvolvimento Java, como IntelliJ IDEA, VsCode, Eclipse ou Spring Tools Suite (STS).

* Administrador de banco de dados: Este projeto utiliza um banco de dados. Recomenda-se o uso de um administrador de banco de dados como PGAdmin ou DBeaver para gerenciar o banco de dados. Certifique-se de ter o seu administrador de banco de dados de escolha instalado em sua máquina.

## Configuração do banco de dados

* Antes de executar a aplicação, certifique-se de configurar o banco de dados. Este projeto utiliza variáveis de ambiente para configurar o banco de dados. Você pode encontrar um exemplo das variáveis de ambiente no arquivo .env.example.

* Crie uma cópia do arquivo .env.example e renomeie-a para .env. Preencha as variáveis de ambiente com as informações correspondentes ao seu banco de dados.

Exemplo de arquivo .env preenchido para PostgreSQL:
```
POSTGRES_USER=seu_usuario
POSTGRES_PASSWORD=sua_senha
POSTGRES_DB=nome_do_banco
DB_URL=jdbc:postgresql://localhost:5432/nome_do_banco
```
## Executando o projeto
1. Clone esse repostiório para a sua máquina local:
```
git clone https://gitlab.com/CaiquePrado/desafio-votacao.git
```
2. Execute o Docker Compose para iniciar o banco de dados PostgreSQL e outros serviços necessários:
```
docker-compose up -d
```
3. Execute a aplicação a partir da classe principal Application.java ou utilizando o comando Gradle:
```
gradle bootRun
```

## Executando testes
1. Abra um terminal
2. Execute o seguinte comando Gradle para executar os testes:
```
gradle test
```

## Acesso ao Swagger
Para acessar a documentação da API através do Swagger, siga os passos abaixo:

1. Certifique-se de que a aplicação esteja em execução localmente. Caso não esteja, inicie a aplicação seguindo as instruções específicas do seu ambiente de desenvolvimento.

2. Abra um navegador da web de sua escolha.

3. Na barra de endereço do navegador, digite o seguinte URL: 
```
http://localhost:8080/v1/swagger-ui/index.html#/
```